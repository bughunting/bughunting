# Testing

## Vagrant

First ensure that you have working Docker instance or that you have Podman with Podman Docker wrapper installed to emulate Docker CLI.
Note that Vagrant with Podman combination is not able to execute containers automatically, there is opened [pull request](https://github.com/hashicorp/vagrant/pull/11356) at Vagrant for this.

You can execute test suite with Vagrant by running:
- `vagrant up --no-parallel`

This will automatically build server and client sequentially i.e. first build and run the server and then the client container. That way the integration test suite will be executed automatically on the client container.

## Containers

Containers can be used in order to test server-client communication and tasks. Both Docker and Podman can be used for building and running the containers.

### Container build

- Server: `$ podman build -f Dockerfile.server ./ -t bughunting/server`

- Client: `$ podman build -f Dockerfile.client ./ -t bughunting/client`

### Running the containers

The containers communicate with each other on 127.0.0.1. It is recommended to run them on host network or on the same bridge.

Run the server container in background:
- `$ podman run --rm --network=host bughunting/server -- /opt/bughunting/servers_run.rb`

You can then kill the container by simply pressing <kbd>Ctrl</kbd><kbd>C</kbd>.

Run the test suite in client container:
- `$ podman run --rm --network=host bughunting/client -- cucumber`

Optionally you can go, fix the code interactively and submit it to the server to check it.
1. start the container interactively:
  - `$ podman run -it --rm --network=host bughunting/client -- bash`
2. fix the code in the container:
~~~
client$ cd ~/sources/warm_up/
# Fix the code
client$ hunt
~~~

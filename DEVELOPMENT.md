# Development

## Architecture
Bughunting server currently uses SCP / SSH calls, with root privileges, to access files on clients, or to make other changes. We currently aim to change that.

### Tasks
* Add build_commands option to define commands that need to be run to build the task. [Done]
* List build commands in the web UI. [TODO]
* After copying files, execute the rest of the jail under the user submitted the task. [Pending]
* Send a patch of sources for check instead of copying the files over SSH [Done]
  * send the whole file as a patch if copy_task_on_server option is false. [Done]
  * create a diff from sources file if copy_task_on_server option is true. [Done]
* Apply the diff or create a new file from the diff. [Done]
* save the patch into a file for archiving [Done]
* diff validation: check for valid files. [Done]
* Create 'apply_patch' option in server config to influence the flow of carrier get method [Done]
  * copy over SCP as it was done so far if the option is false [Done]
  * apply the patch if the option is true [Done]
    * Send binary files from client to server. [Pending]

### Testing tasks
* Allow mounting local directory with tasks that are being developed into containers. [WIP]

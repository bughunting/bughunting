module PatchValidator
  def validate_fail
    raise (@fail_message || 'Patch validation failed.')
  end

  def validate_patch(patch, &block)
    patch
      .reject(&:empty?)
      .map { |diff| validate_diff diff, &block }
  end

  # Validate a diff.
  # The body of a diff cannot contain '\-\-\- ' or '+++ ' otherwise applying
  # the diff will result into error. If the body contains these strings
  # a +RuntimeError+ is raised.
  def validate_diff(diff, &block)
    lines = diff.split $/
    diff_content = lines.drop(2)
    validate_absent diff_content, '--- '
    validate_absent diff_content, '\+\+\+ '

    [
      validate_present(lines[1], '\+\+\+ ', &block)
    ]
  end

  def validate_present(line, expression, &block)
    match = line.scan(/^#{expression}(\S*)\s*.*$/).flatten
    validate_fail unless match.one?
    match = match.first
    if block_given?
      validate_fail unless yield match
    end
    match
  end

  def validate_absent(lines, expression)
    validate_fail unless lines.none? { |line| line.start_with?(expression) }
  end
end

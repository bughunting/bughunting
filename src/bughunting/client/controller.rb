#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/common"
require "bughunting/client/config"
require "bughunting/client/error"
require "bughunting/common/task"
require "bughunting/common/local_execute"
require "pathname"
require "xmlrpc/client"

class ClientController

  include LocalExecute

  attr_accessor :client_config

  def initialize(client_config_file, task_name)
    @client_config = ClientConfig.load_file client_config_file
    @task_name = task_name || autodetect_task!
    @xmlrpc = XMLRPC::Client.new3({
      "host" => @client_config["server.host"],
      "port" => @client_config["server.port"],
      "timeout" => 100
    })
  end

  def check
    $stderr.puts "Submitting task '%s'." % @task_name
    (success, message) = @xmlrpc.call "bughunting.check", @task_name, create_patch
    print_check_result success, message
    success
  end

  def clean
    $stderr.puts "Cleaning task '%s'" % @task_name
    task_cache_sources = File.join(@client_config['paths']['cache_dir'], @task_name, 'source')
    task_path_to_clean = File.join(@client_config['paths']['sources_dir'], @task_name)

    FileUtils.rm_rf task_path_to_clean
    FileUtils.cp_r task_cache_sources, task_path_to_clean
  end

  def serverclean
    $stderr.puts "Cleaning task #{@task_name}"
    @xmlrpc.call 'bughunting.clean', @task_name
  end

  def list
    # ignore @task_name
    task_list = @xmlrpc.call "bughunting.list_tasks"

    columns = `stty size 2>/dev/null`.split[1].to_i
    columns = 80 if columns.nil? or columns < 80

    name_max = 0
    task_list.each do |task_info|
      length = task_info["name"].length
      name_max = length if length > name_max
    end

    summary_max = columns - name_max - 7

    task_list.each do |task_info|
      name = ("%-#{name_max}s" % task_info["name"]).bold
      summary = task_info["summary"][0..summary_max]
      summary += "..." if summary.length != task_info["summary"].length

      puts "%s - %s" % [name, summary]
    end

    true
  end

  def patch
    puts create_patch
  end

  def create_patch
    cache_dir = File.join @client_config['paths']['cache_dir'], @task_name
    task = Task.new cache_dir

    task.config['check_files'].inject([]) do |patch, target_name|
      target_path = File.join @client_config['paths']['sources_dir'], @task_name
      cache_dir_sources = File.join cache_dir, 'source'
      original_file = if task.config['copy_task_on_server']
                        File.join(cache_dir_sources, target_name)
                      else
                        '/dev/null'
                      end

      create_diff(original_file, target_name, target_path, patch)
    end
  end

  # Actually runs `diff` on two specified files in specified directory
  # and appends the output into `patch`.
  def create_diff(original_file, target_file, target_path, patch)
    local_execute(
      [
        "diff", "-Naur",
        original_file,
        target_file
      ],
      work_dir: target_path,
      out: patch
    )
    patch
  end

  def relative_path(origin_path, target_path)
    Pathname.new(target_path).relative_path_from(Pathname.new(origin_path)).to_s
  end

  def autodetect_task!
    cwd = `/bin/pwd -L`.strip
    sources = File.join(File.expand_path(@client_config['paths']['sources_dir']), "/")

    if cwd.start_with? sources
      cwd[sources.length .. -1].split("/").first
    else
      raise ClientError.new "Cannot autodetect the task name. Please, specify manually."
    end
  end

  def print_check_result(success, message)
    $stderr.puts "%s: %s" % [
      success ? "OK".bold.green : "ERROR".bold.red,
      message
    ]
  end
end

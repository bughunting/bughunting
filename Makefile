NAME = bughunting
VERSION = $(shell awk '/^Version:/ {print $$2}' bughunting.spec)
RELEASE = $(shell awk '/^Release:/ {print $$2}' bughunting.spec)
VERSIONED_NAME = $(NAME)-$(VERSION)

DESTDIR=
RUBY_SITELIB = $(shell rpm --eval %ruby_vendorlibdir)

RPMBUILD_DEFINES = --define "_sourcedir `pwd`/rpm-build-dir" --define "_srcrpmdir `pwd`/rpm-build-dir" --define "_rpmdir `pwd`/rpm-build-dir" --define "_specdir `pwd`/rpm-build-dir"

archive:
	git archive --format=tar --prefix $(VERSIONED_NAME)/ HEAD | xz > $(VERSIONED_NAME).tar.xz

install:
	# configuration files
	install -d -m 0755 $(DESTDIR)/etc/bughunting
	install -m 0644 config/server.yml $(DESTDIR)/etc/bughunting/
	install -m 0644 config/client.yml $(DESTDIR)/etc/bughunting/
	install -m 0644 config/web.yml $(DESTDIR)/etc/bughunting/

	# library
	install -d -m 0755 $(DESTDIR)$(RUBY_SITELIB)
	cp -a src/bughunting $(DESTDIR)$(RUBY_SITELIB)/
	chmod -R u=rwX,g=rX,o=rX $(DESTDIR)$(RUBY_SITELIB)/bughunting

	# binaries
	mkdir -p $(DESTDIR)/usr/bin
	install -m 0755 src/hunt.rb $(DESTDIR)/usr/bin/hunt
	install -m 0755 src/huntinstall.rb $(DESTDIR)/usr/bin/huntinstall
	install -m 0755 src/huntlocal.rb $(DESTDIR)/usr/bin/huntlocal
	install -m 0755 src/huntserver.rb $(DESTDIR)/usr/bin/huntserver

	# state directories
	mkdir -p $(DESTDIR)/var/lib
	install -d -m 0755 $(DESTDIR)/var/lib/bughunting
	install -d -m 0755 $(DESTDIR)/var/lib/bughunting/tasks
	install -d -m 0755 $(DESTDIR)/var/lib/bughunting/archive
	install -d -m 0755 $(DESTDIR)/var/lib/bughunting/cache

	# web
	mkdir -p $(DESTDIR)/usr/share
	install -d -m 0755 $(DESTDIR)/usr/share/bughunting
	cp -a web $(DESTDIR)/usr/share/bughunting/
	cp -a scripts/deploy-clients.sh $(DESTDIR)/usr/share/bughunting/
	cp -a scripts/reset-clients.sh $(DESTDIR)/usr/share/bughunting/
	cp -a scripts/get-clients.py $(DESTDIR)/usr/share/bughunting/

srpm: archive
	rm -rf rpm-build-dir
	mkdir rpm-build-dir
	rpmbuild $(RPMBUILD_DEFINES) --nodeps -ts $(VERSIONED_NAME).tar.xz

rpms: srpm
	rpmbuild $(RPMBUILD_DEFINES) --nodeps -tb $(VERSIONED_NAME).tar.xz

clean:
	rm -rf rpm-build-dir
	rm -f *.tar.bz2 *.tar.xz

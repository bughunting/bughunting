#!/bin/sh
# build_tasks target tasks
# ./build_client_tasks.sh [target] [git repo | path]
#
#  target	affects where are tasks installed
#		can be either 'client' or 'server'
#
# Optional
#   git repo	url to the git repo containing tasks
#   path	custom path to the tasks
set -xe
tasks=''
target="$1"
shift

if [ "$1" != "" ] ; then
	is_from_git=$(echo $1 | grep git)
fi

if [ $is_from_git ] ; then
	tasks=$1
	use_git=true

elif [ -d "tasks" ] ; then
	use_git=false
	tasks='tasks'

elif [ $# -eq 0 ] ; then
	tasks='https://gitlab.com/bughunting/tasks.git'
	use_git=true
else
	tasks=$1
	use_git=false
fi

if [ "$use_git" == true ] ; then
	git clone $tasks tasks # should clone into folder named tasks
	tasks=$(basename $tasks | egrep -o '.*[^.git]') # obsolete by previous line
fi

cd $tasks
dnf install -y perl-YAML-Syck
make rpms

if [ $target == "client" ] ; then
	rpm2cpio ./.rpm-build-dir/x86_64/bughunting-tasks* | cpio -idmv

	# Copy the tasks into the client's home
	for task_path in $(ls './var/lib/bughunting/tasks/'); do
		task_name=$(basename $task_path)
		mkdir -p ~/sources/$task_name
		mkdir -p /var/lib/bughunting/cache
		cp -r ./var/lib/bughunting/tasks/$task_name/source/* ~/sources/$task_name/
		cp -r ./var/lib/bughunting/tasks/$task_name /var/lib/bughunting/cache/
	done

elif [ $target == "server" ] ; then
	dnf install -y ./.rpm-build-dir/x86_64/bughunting-tasks*
fi

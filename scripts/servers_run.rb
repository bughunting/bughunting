#!/usr/bin/ruby -d

opts = ENV['RUBYOPT'] ? '' : 'RUBYOPT="-d"'

threads = [
  Thread.new { `#{opts} huntserver` },
  Thread.new { `#{opts} /usr/share/bughunting/web/web.rb` }
]

threads.each { |thread| thread.join }

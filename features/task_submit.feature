Feature: Submitting bughunting task on client
  Background:
    Given a mocked home directory
    # And a bughunting server is running

  Scenario Outline: User submits broken task
    Given I copy a directory "%/<task_name>/source/" to "sources/<task_name>/"
    When I cd to "sources/<task_name>/"
    And I run `hunt check <task_name>`
    Then the output should contain "ERROR: No score points for this. Try again."

    Examples:
      | task_name           |
      | warm_up             |
      | copy_task_on_server |

  Scenario Outline: User submits patched task
    Given I copy a directory "%/<task_name>/source/" to "sources/<task_name>/"
    When I cd to "sources/<task_name>/"
    When I run `patch -f -F 0 -p1` interactively
    And I pipe in the file "%/warm_up/fix.patch"
    And I run `hunt check <task_name>`
    Then the output should contain "OK: You gain 1 score points."

    Examples:
      | task_name           |
      | warm_up             |
      | copy_task_on_server |

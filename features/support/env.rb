require 'aruba/cucumber'

Aruba.configure do |config|
  config.fixtures_directories = %w(tasks)
end

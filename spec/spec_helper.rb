require 'bughunting/common/patch_validator'

class PatchValidatorClass
  include PatchValidator
end

RSpec.shared_context 'global context', :shared_context => :metadata do
  let(:fixtures_path) { File.expand_path './spec/fixtures' }
  let(:script_diff) { File.read File.join(fixtures_path, 'script_diff.patch') }
  let(:script_diff_against_cache) { File.read File.join(fixtures_path, 'script_diff_against_cache.patch')  }
  let(:patch_validator) { PatchValidatorClass.new }
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  # Context included for all tests automatically.
  config.include_context 'global context'

  config.order = :random

  Kernel.srand config.seed
end

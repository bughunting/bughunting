require 'bughunting/common/task_config'

RSpec.describe TaskConfig do
  describe '#get_defaults' do
    context 'copy_task_on_server' do
      it 'is boolean' do
        expect(TaskConfig.get_defaults["copy_task_on_server"]).to be(true).or(be(false))
      end
    end
  end
end

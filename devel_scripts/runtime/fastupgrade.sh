#!/bin/sh

set -x -e

cd ..
make rpms
sudo rpm -Uvh --oldpackage --force rpm-build-dir/noarch/bughunting-server-2012.*.rpm rpm-build-dir/noarch/bughunting-2012.*.rpm

#!/usr/bin/ruby

require "test/unit"
require "bughunting/check/jail"

# fake jail for teardown
class MockJail
  def destroy
  end
end

class TestCheckJail < Test::Unit::TestCase

  # FIXME: hardcoded
  TEST_USER = "bughunting1"

  def setup
    assert_equal 0, Process.euid, "test requires 'root' euid"
    @jail = Jail.new(TEST_USER)
  end

  def teardown
    @jail.destroy
  end

  def test_jail_dir
    assert File.exists? @jail.path
    assert File.directory? @jail.path
    assert File.stat(@jail.path).owned?
    assert_equal 0711, File.stat(@jail.path).mode & 0777
  end

  def test_work_dir
    path = File.join @jail.path, "work"

    assert File.exists? path
    assert File.directory? path
    assert_equal 0700, File.stat(path).mode & 0777

    require "etc"
    assert_equal Etc.getpwnam(TEST_USER).uid, File.stat(path).uid
  end

  def test_log_file
    path = File.join @jail.path, "check.log"

    assert File.exists? path
    assert File.file? path
    assert File.stat(path).owned?
    assert_equal 0600, File.stat(path).mode & 0777
  end

  def test_cleanup
    path = @jail.path
    @jail.destroy
    assert !File.exists?(path)

    @jail = MockJail.new
  end
end

#!/usr/bin/ruby

require "test/unit"
require "bughunting/common/local_execute"

class TestLocalExecuteClass < Test::Unit::TestCase

  # Mock class to simulate real use case
  class Example
    include LocalExecute
  end

  def example
    Example.new
  end

  def argv
    ['ruby', '-v']
  end

  def test_run_accepts_array
    assert example.local_execute(argv)
  end

  def test_run_returns_false_with_bad_command
    assert_equal false, example.local_execute(['false'])
  end

  def test_run_with_debug
    output = capture_output do
      out = ''
      example.local_execute(argv, debug: true, out: out)
      out
    end

    assert_match(/^\+ ruby -v/, output.join($/))
    assert_match(/result: #{$?}/, output.join($/))
  end
end

<script language="javascript" type="text/javascript">
// FIXME: This is really lame try to get a countdown on the page. The date
// should be taken from the file where start game timestamp is written, then
// a run duration (configured value) should be added and whole javascript
// should be moved to an external file where only initialization of
// the countdown could be done here.

// With this implementation the date has to be updated for every game.
// But it at least works.

// set the date we're counting down to
var target_date = new Date("Apr 9, 2014 19:03:00");
 
// variables for time units
var days, hours, minutes, seconds;
 
// get tag element
var countdown = document.getElementById("countdown");
 
// update the tag with id "countdown" every 1 second
setInterval(function () {
 
    // find the amount of "seconds" between now and target
    var current_date = new Date().getTime();
    var seconds_left = (target_date - current_date) / 1000;
 
    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);
     
    // format countdown string + set tag value
    if (seconds_left > 0)
        countdown.innerHTML = "Time remaining: <strong>" + minutes + " minutes " + seconds + " seconds</strong>";  
    else
        countdown.innerHTML = "Time OVER!";
 
}, 1000);
</script> 
